// Copyright 2018 JV

/**
 * @file Cell.hpp
 * @brief      Declaração da Classe Cell, seus atributos e métodos.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      25/08/2018
 * @date       25/08/2018
 */

#ifndef INCLUDE_CELL_HPP_
#define INCLUDE_CELL_HPP_

#include <iostream>
#include <complex>

#include "Point.hpp"

class Cell {
public:
	Point p;
    bool snippet = false;
    bool insect = false;
    bool wall = false;
    bool gate = false;
    std::string gate_answer;

    bool bomb_alert = false;

    int snippet_influence = 0;
    int bug_influence = 0;
    

    int influence() {
        return snippet_influence - bug_influence;
    }

	Cell() {}
	Cell(Point &p): p(p) {}
    Cell(int x, int y): p(x,y) {}

	bool operator==(const Cell &a) const {
		return p == a.p;
	}

    void print_influence() {
        if (wall) {
            std::cerr << "x" << " "; 
            return;  
        }
        std::cerr << influence() << " ";
    }

	void print() const {
        p.print();
		std::cerr << "" << std::endl;
	}

};
#endif // INCLUDE_CELL_HPP_
