// Copyright 2018 JV

/**
 * @file Game.cpp
 * @brief      Implementação da Classe Game.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      13/08/2018
 * @date       18/08/2018
 */

#include <iostream>
#include "Game.hpp"

void Game::process_next_command() {
	std::string command;
	std::cin >> command;
	if (command == "settings") {
		std::string type;
		std::cin >> type;
		if (type == "timebank") {
			std::cin >> timebank;
		} else if (type == "time_per_move") {
			std::cin >> time_per_move;
		} else if (type == "player_names") {
			std::string names;
			std::cin >> names;
			// player names aren't very useful
		} else if (type == "your_bot") {
			std::cin >> me.name;
		} else if (type == "your_botid") {
			std::cin >> me.id;
		} else if (type == "field_width") {
			std::cin >> board.width;
		} else if (type == "field_height") {
			std::cin >> board.height;
		} else if (type == "max_rounds") {
			std::cin >> max_rounds;
		}
	} else if (command == "update") {
		std::string player_name, type;
		std::cin >> player_name >> type;
		if (type == "round") {
			std::cin >> current_round;
		} else if (type == "field") {
			board.clear_influence();
			board.snippets.clear();
			board.weapons.clear();
			board.bugs.clear();
			board.gates.clear();
			board.spawn_points.clear();
/* 			board.is_wall.clear();
			for (int x = 0; x < board.height; x++) {
				board.is_wall.push_back(std::vector<bool>(board.width));
			} */
			std::string s;
			std::cin >> s;
			board.squares.clear();
			while (true) {
				size_t found = s.find(",");
				std::string t;
				if (found == std::string::npos) {
					t = s;
				} else {
					t = s.substr(0, found);
					s.erase(0, found + 1);
				}

				std::vector<std::string> a; a.clear();
				while (true) {
					size_t found = t.find(";");
					if (found == std::string::npos) {
						a.push_back(t);
						break;
					}
					a.push_back(t.substr(0, found));
					t.erase(0, found + 1);
				}
				board.squares.push_back(a);

				if (found == std::string::npos) {
					break;
				}
			}

			for (int x = 0; x < board.height; x++) {
				for (int y = 0; y < board.width; y++) {
					Point pt;
					pt.x = x;
					pt.y = y;
					board.cells[x][y].p = pt;
					int l = x * board.width + y;
					int size = board.squares[l].size();
					for (int i = 0; i < size; i++) {
						std::string c = board.squares[l][i];
						if (c[0] == 'x') {
							board.cells[x][y].wall = true;
							// board.is_wall[x][y] = true;
						} else if (c[0] == '.') {
							//do nothing, is_wall[x][y] == 0 by default
						} else if (c[0] == 'P') {
							// player id
							int id = c[1] - '0';
							if (id == me.id) {
								me.pos.x = x;
								me.pos.y = y;
								board.me.x = x;
								board.me.y = y;
							}
							else {
								enemy.pos.x = x;
								enemy.pos.y = y;
								board.enemy.x = x;
								board.enemy.y = y;
							}
						} else if (c[0] == 'S') {
							if (c != "S") {
								board.spawn_points.push_back(std::make_pair(pt, stoi(c.erase(0, 1))));
							} else {
								board.spawn_points.push_back(std::make_pair(pt, -1));
							}
						} else if (c[0] == 'G') {
							int d;
							switch (c[1]) {
							case 'u': d = 0;
								board.cells[x][y].gate_answer = "up";
								break;
							case 'd': d = 1;
								board.cells[x][y].gate_answer = "down";
								break;
							case 'l': d = 2;
								board.cells[x][y].gate_answer = "left";
								break;
							case 'r': d = 3;
								board.cells[x][y].gate_answer = "right";
								break;
							};
							board.cells[x][y].gate = true;
							board.gates.push_back(std::make_pair(pt, d));
						} else if (c[0] == 'E') { // bug
							switch (c[1]) {
							case '0':
								board.place_bug_influence(board.cells[x][y], 150);
								break;
							case '1':
								board.place_bug_influence(board.cells[x][y], 130);
								break;
							case '2':
								board.place_bug_influence(board.cells[x][y], 110);
								break;
							case '3':
								board.place_bug_influence(board.cells[x][y], 90);
								break;
							};
							board.bugs.push_back(std::make_pair(pt, c[1] - '0'));
						} else if (c[0] == 'B') {
							if (c != "B") { // bomb
								board.place_bomb_alert(board.cells[x][y]);
								board.weapons.push_back(std::make_pair(pt, stoi(c.erase(0, 1))));
							} else {
								board.weapons.push_back(std::make_pair(pt, -1));
							}
						} else if (c[0] == 'C') {
							board.place_snippet_influence(board.cells[x][y], 100);
							board.snippets.push_back(pt);
						}
					}
				}
			}
			//board.print_influence();
			//board.print_squares();

		} else if (type == "snippets") {
			if (player_name == me.name) {
				std::cin >> me.snippets;
			} else {
				std::cin >> enemy.snippets;
			}
		} else if (type == "bombs") {
			if (player_name == me.name) {
				std::cin >> me.bombs;
			} else {
				std::cin >> enemy.bombs;
			}
		}
	} else if (command == "action") {
		std::string action;
		std::cin >> action;
		if (action == "character") {
			std::cin >> time_remaining;
			me.choose_character();
		} else if (action == "move") {
			std::cin >> time_remaining;
			//std::cerr << "			Round: " << current_round << std::endl;
			//computePaths();
			//std::exit(0);
			me.do_move(board);
			board.clear_influence();
		}
	}
}

bool* Game::compute_moves(Point &point) {
	int dx[4] = { -1,1,0,0 };
    int dy[4] = { 0,0,-1,1 };
    //std::string moves[4] = { "up", "down", "left", "right" };

	bool *paths = new bool[4];
	paths[0] = false;
	paths[1] = false;
	paths[2] = false;
	paths[3] = false;

	for (int dir = 0; dir < 4; dir++) {
		int nextx = point.x + dx[dir];
		int nexty = point.y + dy[dir];
		if (nextx >= 0 && nextx < board.height && nexty >= 0 && nexty < board.width) {
			if (!board.cells[nextx][nexty].wall) {
				paths[dir] = true;
			}
		}
	}
	return paths;
}

void Game::computePaths() {
	for (int x = 0; x < 15; ++x) {
		for (int y = 0; y < 19; ++y) {
			Point p = Point(x, y);
			bool *path = compute_moves(p);
			writePaths(x, y, path[0],path[1],path[2],path[3]);
		}
	}
}

void Game::writePaths(int x, int y, bool n_up, bool n_down, bool n_left, bool n_right) {
	std::cout << "cells[" << x << "][" << y << "].path(" << n_up << ", " << n_down << ", " << n_left << ", " << n_right << ");" << std::endl;
}