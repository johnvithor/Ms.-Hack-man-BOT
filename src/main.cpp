#include "Game.hpp"

int main() {
    Game game;
	while (true) {
		game.process_next_command();
	}
	return 0;
}