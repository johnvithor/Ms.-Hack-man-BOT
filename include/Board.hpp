// Copyright 2018 JV

/**
 * @file Board.hpp
 * @brief      Declaração da Classe Board, seus atributos e métodos.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      19/08/2018
 * @date       19/08/2018
 */

#ifndef INCLUDE_BOARD_HPP_
#define INCLUDE_BOARD_HPP_

#include <string>
#include <vector>

#include "Point.hpp"
#include "Cell.hpp"

class Board {
public:
    int width;
    int height;
    Cell cells[15][19];

    std::vector<std::vector<std::string>> squares;
    // std::vector<std::vector<bool>> is_wall; 

    Point me;
    Point enemy;
    std::vector< Point > snippets;
    std::vector< std::pair<Point, int> > weapons;
    std::vector< std::pair<Point, int> > bugs;
    std::vector< std::pair<Point, int> > spawn_points;
    std::vector< std::pair<Point, int> > gates;

    Board();
    bool* get_valid_moves(Point &p);
    void print_influence();
    void place_snippet_influence(Cell &cell, int influence);
    void place_bug_influence(Cell &cell, int influence);
    void place_bomb_alert(Cell &cell);
    void clear_influence();
};

#endif // INCLUDE_BOARD_HPP_