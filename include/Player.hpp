// Copyright 2018 JV

/**
 * @file Player.hpp
 * @brief      Declaração da Classe Player, seus atributos e métodos.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      13/08/2018
 * @date       21/08/2018
 */

#ifndef INCLUDE_PLAYER_HPP_
#define INCLUDE_PLAYER_HPP_

#include <string>
#include <vector>


#include "Point.hpp"
#include "Board.hpp"

class Player {
public:
	Point pos;
	std::string name;
	int id;
	int snippets;
	int bombs;

	bool gated = false;

	std::vector<std::string> next_moves;

	Point snippet_target;
	std::string last_move;
	Point last_pos;

	void choose_character();
    void do_move(Board &board);

	bool follow_influence(Board &board, int valid[]);
	void go_to_closest(Board &board, int valid[]);
	Point get_closest_point_far_from_enemy(std::vector<Point> &points, Point &point, Point &enemy);
	std::pair<std::string, int > closest_move(Board &board, Point &point);
	bool enemy_closer_than_me_into_good_influence(Board &board);
	std::string get_inv(std::string last_move);
	bool gate_used(Board &board);
	std::string place_bomb(Board &board, std::string move, int dir);
	bool danger(Board &board);
};

#endif // INCLUDE_PLAYER_HPP_