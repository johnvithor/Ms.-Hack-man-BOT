// Copyright 2018 JV

/**
 * @file Player.hpp
 * @brief      Implementação da Classe Player.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      13/08/2018
 * @date       21/08/2018
 */

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


#include "Player.hpp"
#include "Board.hpp"

void Player::choose_character() {
	std::cout << "bixie" << std::endl;
//  std::cout << "bixiette" << std::endl;
}

void Player::do_move(Board &board) {
	int valid[4] = {-1,-1,-1,-1};
	if(!next_moves.empty()) {
		std::cout << next_moves.front() << std::endl;
		next_moves.erase(next_moves.begin());
		return;
	}
	if(!enemy_closer_than_me_into_good_influence(board)) {
		if(follow_influence(board, valid)) {
			return;
		}
	}
	if(!gated && gate_used(board)){
		return;
	}
	gated = false;
	go_to_closest(board, valid);
	return;
}

std::string Player::get_inv(std::string last_move) {
	if (last_move == "up") {
		return "down";
	}
	if (last_move == "down") {
		return "up";
	}
	if (last_move == "left") {
		return "right";
	}
	if (last_move == "right") {
		return "left";
	}
	return "pass";
}

bool Player::enemy_closer_than_me_into_good_influence(Board &board) {
	if (board.me.distance(board.enemy) < 5) {
		int my_inf = board.cells[board.me.x][board.me.y].influence();
		int enemy_inf = board.cells[board.enemy.x][board.enemy.y].influence();
		if(my_inf < enemy_inf) {
			return true;
		}
	}
	return false;
}

Point Player::get_closest_point_far_from_enemy(std::vector<Point> &points, Point &p, Point &enemy) {
	if(points.empty()) {
		return p;
	}
	Point closest = points.at(0);
	double minor_distance = p.distance(closest);
	double distance = minor_distance;
	
	for(auto&& point : points) {
		distance = p.distance(point);
		if (distance < minor_distance) {
			if(enemy.distance(point) > distance) {
				minor_distance = distance;
				closest = point;
			}
		}
	}
	return closest;
}

std::pair<std::string, int > Player::closest_move(Board &board, Point &point) {
	int dx[4] = { -1,1,0,0 };
    int dy[4] = { 0,0,-1,1 };
    std::string moves[5] = { "up", "down", "left", "right", "pass" };
	int move = 4;
	Point p;
	double minor = 1000.0;
	for(int i = 0; i < 4; ++i) {
		p.x = pos.x + dx[i];
		p.y = pos.y + dy[i];
		if (p.x >= 0 && p.x < 15 && p.y >= 0 && p.y < 19) {
			if(!board.cells[p.x][p.y].wall) {
				double d = p.distance(point);
				if(d < minor) {
					minor = d;
					move = i;
				}
			}
		}
	}
	return std::make_pair(moves[move],move);
}

bool Player::gate_used(Board &board) {
	if(board.cells[pos.x][pos.y].gate) {
		last_move = board.cells[pos.x][pos.y].gate_answer;
		std::cout << last_move << std::endl;
		//std::cerr << "move final: " << last_move << std::endl;
		gated = true;
		return true;
	}
	return false;
}

bool Player::follow_influence(Board &board, int valid[]) {
	int dx[4] = { -1,1,0,0 };
    int dy[4] = { 0,0,-1,1 };
    std::string moves[5] = { "up", "down", "left", "right", "pass" };
	int valid_i = 0;
	int move = 4;
	int influence = -1000;
	for(int i = 0; i < 4; ++i) {
		int nextx = pos.x + dx[i];
		int nexty = pos.y + dy[i];
		if (nextx >= 0 && nextx < 15 && nexty >= 0 && nexty < 19) {
			int inf = board.cells[nextx][nexty].influence();
			if(!board.cells[nextx][nexty].wall && !board.cells[nextx][nexty].bomb_alert) {
				valid[valid_i++] = i;
				//std::cerr << "inf: " << inf << std::endl;
				//std::cerr << "move: " << moves[i] << std::endl;
				if(inf > influence) {
					influence = inf;
					move = i;
				}
			}
		}
	}
	if(valid_i == 1) {
		if(!gated && gate_used(board)){
			return true;
		}
		gated = false;
	}

	//std::cerr << "influence final : " << influence << std::endl;
	//std::cerr << "last_move: " << last_move << std::endl;
	if (move != 4 && influence != 0) {
		if(bombs != 0 && danger(board)){
			std::cout << place_bomb(board,moves[move], move) << std::endl; // Importante ser o mais rapido o possivel.	
		} else {
			std::cout << moves[move] << std::endl; // Importante ser o mais rapido o possivel.	
		}
		last_move = moves[move];
		//std::cerr << "move final: " << last_move << std::endl;
		return true;
	}
	return false;
}

void Player::go_to_closest(Board &board, int valid[]) {
	std::string moves[5] = { "up", "down", "left", "right", "pass" };
	//std::cerr << "get_closest_point" << std::endl;
	Point p = get_closest_point_far_from_enemy(board.snippets, pos, board.enemy);
	auto result = closest_move(board, p);

	std::string inv = get_inv(last_move);
	if (result.first == inv) {
		//std::cerr << "result == inv" << std::endl;
		for(int i = 0; i < 4; ++i) {
			if(moves[valid[i]] != inv) {
				if(bombs != 0 && danger(board)){
					std::cout << place_bomb(board, moves[valid[i]], valid[i]) << std::endl; // Importante ser o mais rapido o possivel.	
				} else {
					std::cout << moves[valid[i]] << std::endl; // Importante ser o mais rapido o possivel.					
				}	
				last_move = moves[valid[i]];		
				//std::cerr << "move final: " << last_move << std::endl;
				return;
			}
		}
	}
	if(bombs != 0 && danger(board)){
		std::cout << place_bomb(board,result.first, result.second) << std::endl; // Importante ser o mais rapido o possivel.	
	} else {
		std::cout << result.first << std::endl; // Importante ser o mais rapido o possivel.	
	}	
	last_move = result.first;
	//std::cerr << "move final: " << last_move << std::endl;
}

std::string Player::place_bomb(Board &board, std::string move, int dir) {
	int dx[4] = { -1,1,0,0 };
    int dy[4] = { 0,0,-1,1 };
	std::string bomb = ";drop_bomb ";
	Point next_pos;
	bool ok_2 = false;
	next_pos.x = board.me.x + dx[dir];
	next_pos.y = board.me.y + dy[dir];
	int timer = 2;
	Point p;
	for(int i = 0; i < 4; ++i) {
		p.x = next_pos.x + dx[i];
		p.y = next_pos.y + dy[i];
		if(board.cells[p.x][p.y].wall) {
			//p.print();
			//std::cerr << "parede " << i << std::endl;
			continue;
		}
		if(i < 2) {
			if (p.y-1 >= 0) {
				if (!board.cells[p.x][p.y-1].wall && board.cells[p.x][p.y-1].bug_influence < 80) {
					ok_2 = true;
					if(i == 0) {
						next_moves = {"up", "left"};
					} else {
						next_moves = {"down", "left"};
					}
					break;
				}	
			}
			if (p.y+1 < 19) {
				if (!board.cells[p.x][p.y+1].wall && board.cells[p.x][p.y+1].bug_influence < 80) {
					ok_2 = true;
					if(i == 0) {
						next_moves = {"up", "right"};
					} else {
						next_moves = {"down", "right"};
					}
					break;
				}
			}
		} else {
			if (p.x-1 >= 0) {
				if (!board.cells[p.x-1][p.y].wall && board.cells[p.x-1][p.y].bug_influence < 80) {
					ok_2 = true;
					if(i == 2) {
						next_moves = {"left", "up"};
					} else {
						next_moves = {"right", "up"};
					}
					break;
				}
			}
			if (p.x+1 < 15) {
				if (!board.cells[p.x+1][p.y].wall && board.cells[p.x+1][p.y].bug_influence < 80) {
					ok_2 = true;
					if(i == 2) {
						next_moves = {"left", "down"};
					} else {
						next_moves = {"right", "down"};
					}
					break;
				}
			}
		}
	}
	if(ok_2) {
		bomb += std::to_string(timer);
		return move + bomb;
	}
	return move;
}

bool Player::danger(Board &board) {
	int dx[4] = { -1,1,0,0 };
    int dy[4] = { 0,0,-1,1 };
	Point p;
	int influence = 0;
	for(int i = 0; i < 4; i++) {
		p.x = pos.x + dx[i];
		p.y = pos.y + dy[i];
		influence += board.cells[p.x][p.y].bug_influence;
	}
	
	return influence > 100;
}
