# Makefile baseado no exemplo dado pelo Professor Silvio Costa Sampaio

# $@ nome do alvo (target)
# $^ lista com os nomes de todos os pre-requisitos sem duplicatas
# $< nome do primeiro pre-requisito

# Comandos do sistema operacional
RM = rm -rf

# Compilador
CC=g++

# Variaveis para os subdiretorios
LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test

# Opcoes de compilacao  -I$(INC_DIR)
CFLAGS = -Wall -pedantic -ansi -std=c++1y 

.PHONY: all clean

all: dir bot.exe

debug: CFLAGS += -g -O0 -pg
debug: dir bot.exe

bot.exe: $(OBJ_DIR)/Player.o $(OBJ_DIR)/Board.o $(OBJ_DIR)/Game.o $(OBJ_DIR)/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel bot.exe criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/Player.o: $(SRC_DIR)/Player.cpp 
	$(CC) -c $(CFLAGS) -I$(INC_DIR)/ -o $@ $<

$(OBJ_DIR)/Board.o: $(SRC_DIR)/Board.cpp
	$(CC) -c $(CFLAGS) -I$(INC_DIR)/ -o $@ $<	

$(OBJ_DIR)/Game.o: $(SRC_DIR)/Game.cpp
	$(CC) -c $(CFLAGS) -I$(INC_DIR)/ -o $@ $<	

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) -c $(CFLAGS) -I$(INC_DIR)/ -o $@ $<

dir:
	mkdir -p bin build doc
	
valgrind_1:
	valgrind -v --leak-check=full --show-reachable=yes ./bin/bot.exe

clean: dir
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*

# FIM do Makefile