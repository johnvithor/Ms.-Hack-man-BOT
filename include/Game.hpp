// Copyright 2018 JV

/**
 * @file Game.hpp
 * @brief      Declaração da Classe Game, seus atributos e métodos.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      13/08/2018
 * @date       13/08/2018
 */

#ifndef INCLUDE_GAME_HPP_
#define INCLUDE_GAME_HPP_

#include <string>
#include <vector>
#include "Point.hpp"
#include "Player.hpp"
#include "Board.hpp"

class Game {
public:
    int timebank;
    int time_per_move;
    int time_remaining;
    int max_rounds;
    int current_round;

    Board board;
    Player me;
    Player enemy;

    void process_settings();
    void process_next_action();
    void clear_fields();
    void process_next_command();

    bool* compute_moves(Point &point);
    void computePaths();
    void writePaths(int x, int y, bool n_up, bool n_down, bool n_left, bool n_right);
};

#endif // INCLUDE_GAME_HPP_