// Copyright 2018 JV

/**
 * @file Point.hpp
 * @brief      Declaração da Classe Point, seus atributos e métodos.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      13/08/2018
 * @date       13/08/2018
 */

#ifndef INCLUDE_POINT_HPP_
#define INCLUDE_POINT_HPP_

#include <iostream>
#include <complex>

class Point {
public:
	int x;
	int y;
	Point() {}
	Point(int x, int y): x(x), y(y) {}

	bool operator<(const Point &a) const {
		return x < a.x || (x == a.y && y < a.y);
	}

	bool operator==(const Point &a) const {
		return x == a.x && y == a.y;
	}

	bool operator!=(const Point &a) const {
		return !(*this == a);
	}

	void print() const {
		std::cerr << "(" << x << "," << y << ")" << std::endl;
	}

	int distance(Point p) {
		return (abs(x - p.x) + abs(y - p.y)) * 10;
	//	return sqrt((x - p.x)*(x - p.x) + (y - p.y)*(y - p.y));
	}

	int distance(int px, int py) {
		return (abs(x - px) + abs(y - py)) * 10;
	//	return sqrt((x - p.x)*(x - p.x) + (y - p.y)*(y - p.y));
	}
};
#endif // INCLUDE_POINT_HPP_
