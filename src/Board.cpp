// Copyright 2018 JV

/**
 * @file Board.cpp
 * @brief      Implementação da Classe Board.
 *
 * @author     João Vítor Venceslau Coelho
 * @since      26/08/2018
 * @date       01/09/2018
 */

#include <iostream>
#include "Board.hpp"

void Board::print_influence() {
    for(int x = 0; x < 15; x++) {
        for(int y = 0; y < 19; y++) {
            cells[x][y].print_influence();
        }
        std::cerr << std::endl;
    }
    std::cerr << std::endl;
}

void Board::place_snippet_influence(Cell &cell, int influence) {
    if (influence < 0 || cell.wall) {
        return;
    }
    if(cell.snippet_influence < influence) {
        //std::cerr << "modificado: " << cell.p.x << " " << cell.p.y << " inf:  " << cell.snippet_influence << std::endl;
        cell.snippet_influence = influence;
        //std::cerr << "para: " << cell.snippet_influence << std::endl;
    }

    int dx[4] = { -1,0,1,0 };
    int dy[4] = { 0,-1,0,1 };

    for(int i = 0; i < 4; i++) {
        int x = cell.p.x + dx[i];
        int y = cell.p.y + dy[i];
        if (x >= 0 && x < 15 && y >= 0 && y < 19) {
            place_snippet_influence(cells[x][y], influence - 8);
        }
    }
}

void Board::place_bug_influence(Cell &cell, int influence) {
    if (influence < 0 || cell.wall) {
        return;
    }
    if(cell.bug_influence < influence) {
        //std::cerr << "modificado: " << cell.p.x << " " << cell.p.y << " inf:  " << cell.bug_influence << std::endl;
        cell.bug_influence = influence;
        //std::cerr << "para: " << cell.bug_influence << std::endl;
    }
    int dx[4] = { -1,0,1,0 };
    int dy[4] = { 0,-1,0,1 };

    for(int i = 0; i < 4; i++) {
        int x = cell.p.x + dx[i];
        int y = cell.p.y + dy[i];
        if (x >= 0 && x < 15 && y >= 0 && y < 19) {
            place_bug_influence(cells[x][y], influence - 10);
        }
    }
}

Board::Board() {
    for(int x = 0; x < 15; x++) {
        for(int y = 0; y < 19; y++) {
            cells[x][y].snippet_influence = 0;
            cells[x][y].bug_influence = 0;
            cells[x][y].bomb_alert = false;;
            cells[x][y].gate = false;
            cells[x][y].wall = false;
            cells[x][y].insect = false;
            cells[x][y].snippet = false;
            cells[x][y].p.x = x;
            cells[x][y].p.y = y;
        }
    }
}

void Board::clear_influence() {
    for(int x = 0; x < 15; x++) {
        for(int y = 0; y < 19; y++) {
            cells[x][y].snippet_influence = 0;
            cells[x][y].bug_influence = 0;
            cells[x][y].bomb_alert = false;;
        }
    }
}


void Board::place_bomb_alert(Cell &cell) {    
    for(int i = cell.p.x; i < 15; ++i) {
        if(cells[i][cell.p.y].wall) {
            break;
        }
        cells[i][cell.p.y].bomb_alert = true;
    }
    
    for(int i = cell.p.x; i >= 0; --i) {
        if(cells[i][cell.p.x].wall) {
            break;
        }
        cells[i][cell.p.x].bomb_alert = true;
    }

    for(int i = cell.p.y; i < 19; ++i) {
        if(cells[cell.p.x][i].wall) {
            break;
        }
        cells[cell.p.x][i].bomb_alert = true;
    }
    
    for(int i = cell.p.y; i >= 0; --i) {
        if(cells[cell.p.x][i].wall) {
            break;
        }
        cells[cell.p.x][i].bomb_alert = true;
    }
}